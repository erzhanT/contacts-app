import React from 'react';
import Layout from "./components/UI/Layout/Layout";
import {Switch, Route} from 'react-router-dom';
import PhoneBook from "./containers/PhoneBook/PhoneBook";
import AddContact from "./containers/AddContact/AddContact";
import EditContact from "./containers/EditContact/EditContact";

const App = () => (
    <Layout>
        <Switch>
            <Route path='/' exact component={PhoneBook}/>
            <Route path='/new' exact component={AddContact}/>
            <Route path='/edit/:id' exact component={EditContact}/>
        </Switch>
    </Layout>
);

export default App;
