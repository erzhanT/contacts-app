import {
    FETCH_ONE_CONTACT,
    FETCH_PHONE_FAILURE,
    FETCH_PHONE_REQUEST,
    FETCH_PHONE_SUCCESS, GET_ONE_ID,
    SET_MODAL,

} from "../actions/phoneAction";

const initialState = {
    phones: [],
    oneContact: null,
    openModal: false,
    oneId: null
};

export const phoneReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PHONE_REQUEST:
            return {...state};
        case FETCH_PHONE_SUCCESS:
            return {...state, phones: action.phones};
        case FETCH_PHONE_FAILURE:
            return {...state};
        case FETCH_ONE_CONTACT:
            return {...state, oneContact: action.data};
        case SET_MODAL:
            return {...state, openModal: action.open};
        case GET_ONE_ID:
            return {...state, oneId: action.data};
        default:
            return state;
    }
}