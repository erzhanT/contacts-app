import axios from 'axios';

const url = 'https://phone-book-erzhan-default-rtdb.firebaseio.com/phones.json';

export const FETCH_PHONE_REQUEST = 'FETCH_PHONE_REQUEST';
export const FETCH_PHONE_SUCCESS = 'FETCH_PHONE_SUCCESS';
export const FETCH_PHONE_FAILURE = 'FETCH_PHONE_FAILURE';
export const ADD_CONTACT = 'ADD_CONTACT';
export const FETCH_ONE_CONTACT = 'FETCH_ONE_CONTACT';
export const SET_MODAL = 'SET_MODAL';
export const GET_ONE_ID = 'GET_ONE_ID';

export const fetchPhoneRequest = () => ({type: FETCH_PHONE_REQUEST});
export const fetchPhoneSuccess = phones => ({type: FETCH_PHONE_SUCCESS, phones});
export const fetchPhoneFailure = () => ({type: FETCH_PHONE_FAILURE});
export const addContact = data => ({type: ADD_CONTACT, data});
export const fetchOneContact = data => ({type: FETCH_ONE_CONTACT, data});
export const setModal = open => ({type: SET_MODAL, open});
export const getOneId = id => ({type: getOneId, id});


export const fetchPhones = () => {
    return async dispatch => {

        try {
            const response = await axios.get(url);
            const phones = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));
            dispatch(fetchPhoneSuccess(phones));
        } catch (e) {
            console.log(e);
        }
    };
};

export const addOne = (data, history) => {
    return async dispatch => {
        try {
            await axios.post(url, data);
            dispatch(addContact(data));
        } finally {
            history.push('/')
        }
    };
};

export const getOneContact = id => {
    return async dispatch => {
        try {
            const response = await axios.get('https://phone-book-erzhan-default-rtdb.firebaseio.com/phones/' + id + '.json');
            console.log(response.data);
            dispatch(fetchOneContact(response.data));
        } catch (e) {
            console.error(e);
        }
    }
}


export const editPhone = (id, data, history) => {
    return async dispatch => {
        try {
            await axios.put('https://phone-book-erzhan-default-rtdb.firebaseio.com/phones/' + id + '.json', data);
        } finally {
            history.push('/')
        }
    }
}

export const removeContact = (id) => {
    return async dispatch => {
        try {
            await axios.delete('https://phone-book-erzhan-default-rtdb.firebaseio.com/phones/' + id + '.json');
        } catch (e) {
            console.error(e);
        }
    }
}