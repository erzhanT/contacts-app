import React from 'react';
import './Form.css';
import {Avatar} from "@material-ui/core";


const Form = (props) => {

    return (
        <>

            <form
                onSubmit={props.onsubmit}
                className="form-style-9">
                <ul>
                    <li>
                        <input onChange={props.onchange}
                               type="text"
                               name="firstName"
                               value={props.post.firstName}
                               className="field-style field-split align-left"
                               placeholder="First Name"/>
                        <input
                            onChange={props.onchange}
                            type="text"
                            name="lastName"
                            value={props.post.lastName}
                            className="field-style field-split align-right"
                            placeholder="Last Name"/>

                    </li>
                    <li>
                        <input
                            onChange={props.onchange}
                            type="number"
                            name="number"
                            value={props.post.number}
                            className="field-style field-split align-left"
                            placeholder="Phone"/>
                        <input
                            onChange={props.onchange}
                            type="email"
                            value={props.post.email}
                            name="email"
                            className="field-style field-split align-right"
                            placeholder="Email"/>
                    </li>
                    <li>
                        <input
                            onChange={props.onchange}
                            type="url"
                            name='image'
                            value={props.post.image}
                            className="field-style field-full align-none"
                            placeholder="Image"/>
                    </li>
                </ul>
                <Avatar
                    className='avatar'
                    src={props.post.image}
                />
                <button color="primary">Save</button>
            </form>
        </>
    );
};

export default Form;