import React from 'react';
import {createStyles, makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import './Contact.css';


const useStyles = makeStyles(
    createStyles({
        root: {
            marginTop: '100px',
            display: 'flex',
            paddingTop: '25px',
            paddingBottom: '25px'
        },
        details: {
            display: 'flex',
            flexDirection: 'column',
        },
        content: {
            flex: '1 0 auto',
        },
        cover: {
            width: 151,
        },

    }),
);

const Contact = ({image, firstName, lastName, id, modal}) => {
    const classes = useStyles();

    return (
        <Card className={classes.root} onClick={modal} key={id}>
            <CardMedia
                className={classes.cover}
                image={image}
                title="Image here"
            />
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="h5" variant="h5">
                        {firstName} {lastName}
                    </Typography>
                </CardContent>
            </div>
        </Card>
    );
};
export default Contact
