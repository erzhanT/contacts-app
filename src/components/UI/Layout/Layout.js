import React from 'react';
import {AppBar, CssBaseline, Grid, Toolbar, Typography} from "@material-ui/core";
import './Layout.css'
import {NavLink} from "react-router-dom";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline />
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justify="space-between">
                        <Grid item>
                            <Typography variant="h6" noWrap>
                                Phone Book
                            </Typography>
                        </Grid>
                        <Grid item>
                            <NavLink className='link' to="/new" color="inherit">Add new contact</NavLink>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <main>
                {children}
            </main>

        </>
    );
};

export default Layout;