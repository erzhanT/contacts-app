import React from 'react';
import {createStyles, makeStyles} from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {NavLink} from "react-router-dom";

const useStyles = makeStyles(
    createStyles({
        root: {
            marginTop: '100px',
            display: 'flex',
            paddingTop: '25px',
            paddingBottom: '25px'
        },
        details: {
            display: 'flex',
            flexDirection: 'column',
        },
        content: {
            flex: '1 0 auto',
        },
        cover: {
            width: 151,
        },

    }),
);

const ContactSummary = (props) => {

    const classes = useStyles();
    return (
        <Card className={classes.root} id={props.id}>
            <CardMedia
                className={classes.cover}
                image={props.oneContact.image}
                title="Image here"
            />
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component='h5' variant='h5'>
                        {props.oneContact.firstName} {props.oneContact.lastName}
                    </Typography>
                    <Typography component='h5' variant='h5'>
                        {props.oneContact.number}
                    </Typography>
                    <Typography component='h5' variant='h5'>
                        {props.oneContact.email}
                    </Typography>
                </CardContent>
            </div>
            <NavLink to={`/edit/${props.id}`}>Edit</NavLink>
            <button onClick={props.remove}>Delete</button>
            <button onClick={props.modalCancelled}>EXIT</button>
        </Card>
    );
};

export default ContactSummary;