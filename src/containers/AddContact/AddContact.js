import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {addOne} from "../../store/actions/phoneAction";
import Form from "../../components/Form/Form";

const AddContact = (props) => {

    const dispatch = useDispatch();


    const [post, setPost] = useState({
        firstName: '',
        lastName: '',
        email: '',
        number: '',
        image: ''
    });

    const postContact = (e) => {
        e.preventDefault();
        dispatch(addOne(post, props.history));
    };


    const onChange = (e) => {
        const {name, value} = e.target;
        setPost(prevState => ({
            ...prevState,
            [name]: value
        }));
    }


    return (
        <>
            <Form
                onsubmit={postContact}
                onchange={onChange}
                post={post}
            />
        </>
    );
};

export default AddContact;