import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getOneContact, editPhone} from "../../store/actions/phoneAction";
import Form from "../../components/Form/Form";

const EditContact = (props) => {

    const id = props.match.params.id
    const dispatch = useDispatch();
    const oneContact = useSelector(state => state.oneContact);

    const [edit, setEdit] = useState({});


    useEffect(() => {
        dispatch(getOneContact(id));
    }, [dispatch, id]);

    useEffect(() => {
        setEdit(oneContact);
    }, [oneContact]);

    const onChangeContact = (e) => {
        const {name, value} = e.target;
        setEdit(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const saveHandler = (e) => {
        e.preventDefault();
        dispatch(editPhone(id, edit, props.history));

    }

    return (
        <div>
            {edit && <>
                <Form
                    onchange={onChangeContact}
                    id={id}
                    post={edit}
                    onsubmit={(e) => saveHandler(e)}
                />
            </>}
        </div>
    );
};

export default EditContact;