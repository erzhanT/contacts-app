import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhones, getOneContact, removeContact, setModal} from "../../store/actions/phoneAction";
import {Container} from "@material-ui/core";
import Contact from "../../components/Contact/Contact";
import ContactSummary from "../../components/ContactSummary/ContactSummary";
import Modal from "../../components/UI/Modal/Modal";

const PhoneBook = (props) => {

    const dispatch = useDispatch();
    const phones = useSelector(state => state.phones);
    const openModal = useSelector(state => state.openModal)
    const oneContact = useSelector(state => state.oneContact);
    const [stateId, setId] = useState(null);


    useEffect(() => {
        dispatch(fetchPhones());
    }, [dispatch]);




    const modalHandler = async (id) => {
        await dispatch(getOneContact(id))
        dispatch(setModal(true));
        setId(id)
    };

    const modalCancelHandler = () => {
        dispatch(setModal(false));
    };

    const deleteHandler = () => {
        dispatch(removeContact(stateId))
    }

    return (
        <Container>
            <div className="Phones">
                {oneContact && <Modal
                    show={openModal}
                    closed={modalCancelHandler}
                >
                    <ContactSummary
                        oneContact={oneContact}
                        modalCancelled={modalCancelHandler}
                        remove={deleteHandler}
                        id={stateId}
                    />
                </Modal>}
                {phones && phones.map(phone => {
                    return (
                        <Contact
                            key={phone.id}
                            firstName={phone.firstName}
                            lastName={phone.lastName}
                            image={phone.image}
                            modal={() => modalHandler(phone.id)}
                            id={phone.id}
                        />
                    )
                })}
            </div>

        </Container>

    );
};

export default PhoneBook;